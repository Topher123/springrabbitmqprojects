# SpringRabbitMQProjects

RabbitMQ practice

RabbitMQ Login:
* User name: rabbit
* Password: rabbit

RabbitMQ Local management URL:
* http://localhost:15672

Start RabbitMQ container from project root:
* cd docker
* docker-compose up rabbitmq