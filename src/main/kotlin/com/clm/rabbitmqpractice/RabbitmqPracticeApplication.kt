package com.clm.rabbitmqpractice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RabbitmqPracticeApplication

fun main(args: Array<String>) {
	runApplication<RabbitmqPracticeApplication>(*args)
}
